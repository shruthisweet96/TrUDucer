package nats.truducer.startup;

import cz.ufal.udapi.core.Node;
import cz.ufal.udapi.core.Root;
import nats.truducer.data.*;
import nats.truducer.deprel.CoverageChecker;
import nats.truducer.deprel.PrecisionStats;
import nats.truducer.deprel.TreeComparator;
import nats.truducer.deprel.TreeConversionStats;
import nats.truducer.exceptions.BlockedInteractionException;
import nats.truducer.groovy.GroovyDictionary;
import nats.truducer.gui.ConvGUIController;
import nats.truducer.gui.SessionGUIController;
import nats.truducer.gui.MainWindowController;
import nats.truducer.io.TreeReaderWriter;
import nats.truducer.io.ruleparsing.TransducerLexer;
import nats.truducer.io.ruleparsing.TransducerParser;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.action.StoreTrueArgumentAction;
import net.sourceforge.argparse4j.inf.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static nats.truducer.io.TreeReaderWriter.fileToTree;
import static nats.truducer.io.TreeReaderWriter.treeToFile;

/**
 * Created by felix on 12/01/17.
 */
public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);
    private static int conversionTreeSize;

    private static Map<String, String> PoSTable = null;
    private static Map<String, String> morphologicalFeatureTable = null;
    private static Map<String, String> PoSTableExceptions = null;

    public static void main(String[] args) throws Exception {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("TrUDucer").defaultHelp(true)
                .description("TrUDucer - Transforming to Universal Dependencies with transducers.\n" +
                        "\n" +
                        "Tree transducer based dependency tree annotation schema conversion.\n" +
                        "A single CoNLL file or a whole directory can be converted based on given rule file; " +
                        "can compare directories, useful for testing the correctness of a rulefile with previously annotated trees." +
                        "Test the coverage of a rulefile with 'coverage' or look at a conversion process in detail with 'show'.\n\n" +
                        "For the compare and coverage subcommand it is assumed that the source dependency relations are in CAPS, " +
                        "while the UD labels are in all lowercase.  Otherwise no assumptions about the labels are made.");

        Subparsers subparsers = parser.addSubparsers().dest("subparser_name")
                .help("The various subcommands.");

        Subparser conv = subparsers.addParser("conv")
                .help("Convert a single tree given by a CoNLL file.");
        conv.addArgument("rulefile")
                .help("The file containing the transformation rules.");
        conv.addArgument("input_file")
                .help("The CoNLL file with the dependency tree to be converted.");
        conv.addArgument("output_file")
                .help("The filename of the file to be generated.");
        conv.addArgument("--pos_table")
                .nargs(1)
                .help("the file containing the lookup table for Part of Speech Tag conversion");
        conv.addArgument("--feat_table")
                .nargs(1)
                .help("the file containing the lookup table for morphological feature conversion");

        Subparser convall = subparsers.addParser("convall")
                .help("Convert a whole directory of CoNLL files.");
        convall.addArgument("rulefile")
                .help("The file containing the transformation rules.");
        convall.addArgument("input_dir")
                .help("The directory containing the CoNLL files to be converted.");
        convall.addArgument("output_dir")
                .help("The directory where the converted files should be generated.");
        convall.addArgument("--pos_table")
                .nargs(1)
                .help("the file containing the lookup table for Part of Speech Tag conversion");
        convall.addArgument("--feat_table")
                .nargs(1)
                .help("the file containing the lookup table for morphological feature conversion");

        Subparser test = subparsers.addParser("compare")
                .help("Compare two directories of CoNLL files.");
        test.addArgument("expected_dir")
                .help("The directory containing the CoNNL files with the expected tree structures.");
        test.addArgument("actual_dir")
                .help("The directory containing the CoNLL files with the actual tree structures.");
        test.addArgument("original_dir")
                .help("The directory containing the source CoNLL files.");

        Subparser coverage = subparsers.addParser("coverage")
                .help("Check how many dependency relations are converted; in a single directory.");
        coverage.addArgument("dir")
                .help("The directory containing CoNLL files to be checked for completeness of conversion.");
        coverage.addArgument("original_dir")
                .help("The directory containing the original conll files.");

        Subparser showTree = subparsers.addParser("show")
                .help("Show the conversion process of a single tree step by step in a GUI.");
        showTree.addArgument("input_file")
                .help("The CoNNL file containing the tree to be converted.");
        showTree.addArgument("transducer_file")
                .nargs("?")
                .help("The rule file.");

        Subparser listTrees = subparsers.addParser("list")
                .help("List trees and subsets of trees from the treebank.");
        listTrees.addArgument("src_dir")
                .help("The directory containing the original conll files.");
        listTrees.addArgument("gen_dir")
                .help("The directory containing the generated conll files.");
        listTrees.addArgument("-i", "--incomplete").action(new StoreTrueArgumentAction())
                .help("only list trees that are not fully converted");
        listTrees.addArgument("-b", "--blockers").action(new StoreTrueArgumentAction())
                .help("list the blockers in the trees");

        Subparser searchTrees = subparsers.addParser("search")
                .help("Search for trees by giving a substructure of the tree.");
        searchTrees.addArgument("dir")
                .help("Directory containing the tree files to search.");
        searchTrees.addArgument("expr")
                .help("The search expression given in the TrUDucer rule syntax.");

        Subparser developmentSession = subparsers.addParser("session")
                .help("starts a graphical tool for developing conversion rules");
        developmentSession.addArgument("-t", "--transducer")
                .nargs(1)
                .help("The file containing the transformation rules.");
        developmentSession.addArgument("--treecount")
                .type(Integer.class)
                .setDefault(0)
                .help("how many trees of the treebank to convert (\"0\" to convert all)");
        developmentSession.addArgument("input_dir")
                .help("The directory containing the CoNLL files to be converted.");
        developmentSession.addArgument("output_dir")
                .help("The directory where the converted files should be generated.");
        developmentSession.addArgument("--validation")
                .nargs(1)
                .help("the directory containing a validation set of converted CoNLL files");
        developmentSession.addArgument("--pos_table")
                .nargs(1)
                .help("the file containing the lookup table for Part of Speech Tag conversion");
        developmentSession.addArgument("--feat_table")
                .nargs(1)
                .help("the file containing the lookup table for morphological feature conversion");
        developmentSession.addArgument("-v", "--verbose")
                .action(new StoreTrueArgumentAction())
                .help("log debug info");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);

            switch(ns.getString("subparser_name")) {
                case "conv":
                    convertMain(ns);
                    break;
                case "convall":
                    convertDirMain(ns);
                    break;
                case "compare":
                    testMain(ns);
                    break;
                case "coverage":
                    checkCoverageMain(ns);
                    break;
                case "show":
                    showTreeMain(ns);
                    break;
                case "list":
                    listMain(ns);
                    break;
                case "search":
                    searchMain(ns);
                    break;
                case "session":
                    sessionMain(ns);
                    break;
            }
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
    }

    private static void convertMain(Namespace ns) throws Exception {
        logger.info("Starting conversion");
        String transducerPath = ns.getString("rulefile");
        String inPath = ns.getString("input_file");
        String outPath = ns.getString("output_file");

        String posPath = ns.getString("pos_table");
        if(posPath != null && !posPath.equals("")) {
            generatePoSTable(posPath);
        }
        String featPath = ns.getString("feat_table");
        if(featPath != null && !featPath.equals("")) {
            generateFeatTable(featPath);
        }


        Transducer t = pathToTransducer(transducerPath);

        // ** added by Maximilian
        // creates a viewer for interactive Conversions and sets it for all the rules
        ConvGUIController interactiveWindow = new ConvGUIController();
        interactiveWindow.initWindow();

        interactiveWindow.setTransducer(t);
        GroovyDictionary groovyDictionary = new GroovyDictionary();
        for (Rule r : t.rules) {
            r.setGroovyDictionary(groovyDictionary);
        }

        convertFile(t, new File(inPath), new File(outPath));

        interactiveWindow.close();
    }

    private static void convertDirMain(Namespace ns) throws Exception {
        String posPath = ns.getString("pos_table");
        if(posPath != null && !posPath.equals("")) {
            generatePoSTable(posPath);
        }
        String featPath = ns.getString("feat_table");
        if(featPath != null && !featPath.equals("")) {
            generateFeatTable(featPath);
        }

        convertDir(pathToTransducer(ns.getString("rulefile")),
                ns.getString("input_dir"),
                ns.getString("output_dir"));
    }

    public static void convertDir(Transducer t, String inDir, String outDir) {
        convertDir(t, inDir, outDir, false, null);
    }

    public static void convertDir(Transducer t, String inDir, String outDir, boolean checkRulesUsed, SessionGUIController controller) {
        if (!new File(outDir).exists())
            new File(outDir).mkdirs();

        t.storeRulesUsed = checkRulesUsed;

        // ** added by Maximilian
        // same as in convertMain!
        ConvGUIController interactiveWindow = new ConvGUIController();
        interactiveWindow.initWindow();

        interactiveWindow.setTransducer(t);

        CoverageChecker cc = new CoverageChecker();

        // interactive conversions block the conversion process.
        // better is be to convert in two phases, first all conversions
        // that don't need user input, and only after that convert those that
        // use interactive conversions. Then the user is free to move while
        // the machine computes for most of the time.

        // these are all the files requiring interaction, those will be
        // converted in a second pass, after all other conversions are done.
        List<File> interactiveFiles = new ArrayList<>();

        // ** changed by Maximilian
        // now first converts all non interactive files, catches those that
        // require interactions and does those in a second run
        interactiveWindow.setInteractiveAllowed(false);

        String[] fileNames = TreeReaderWriter.getFileNames(new File(inDir));

        int i = 0;
        if(conversionTreeSize == 0)
            conversionTreeSize = fileNames.length;

        for (String paths: fileNames) {
            File file = new File(inDir, paths);
            i++;
            if(i > conversionTreeSize) {
                break;
            }

            try {
                convertFile(t, file, new File(outDir, file.getName()));
            } catch (BlockedInteractionException e) {
                interactiveFiles.add(file);
                logger.info("conversion requires interaction, doing this in second run!");
                continue;
            } catch (Exception e) {
                logger.info("conversion failed, skipping");
                continue;
            }

            TreeConversionStats stats = cc.checkTree(fileToTree(file), fileToTree(new File(outDir, file.getName())));
            if(controller != null && (checkRulesUsed || !stats.isTreeFullyConverted())) {
                stats.setRulesUsed(t.rulesUsed);
                controller.add(new File(outDir, file.getName()), file, stats, true);
            }
        }

        // now convert all files which require interaction with user
        interactiveWindow.setInteractiveAllowed(true);
        for (File file : interactiveFiles) {
            try {
                convertFile(t, file, new File(outDir, file.getName()));
            } catch (Exception e) {
                logger.info("conversion failed, skipping");
                continue;
            }

            TreeConversionStats stats = cc.checkTree(fileToTree(file), fileToTree(new File(outDir, file.getName())));
            if(controller != null && (checkRulesUsed || !stats.isTreeFullyConverted())) {
                stats.setRulesUsed(t.rulesUsed);
                controller.add(new File(outDir, file.getName()), file, stats, true);
            }
        }

        if(controller != null) {

            int blockers = cc.getTotalBlockerCount();

            int convertedSentences = cc.getConvertedTreeCount();
            int sentencesCount = cc.getTotalTreeCount();

            // Perform relevant calculations.
            double failedSentencePercentage = 1.0 - (double) convertedSentences / (double) sentencesCount;

            controller.setPercentage(failedSentencePercentage * 100d);
            Map<String, Integer> blockersIndividual = cc.getBlockersIndividual();
            for(String blocker: blockersIndividual.keySet()) {
                controller.setPercentage(blocker, 100d * (double) blockersIndividual.get(blocker) / (double) blockers);
            }
            controller.sortTree();
        }

        interactiveWindow.close();
        t.runCleanupScript();
    }

    public static void convertFile(Transducer t, File inFile, File outFile) {
        logger.info(String.format("Testing file %s", inFile));
        Root orig = fileToTree(inFile);
        Root transduced = t.applyTo(orig);

        // if desired, also convert PoS tags with given lookup table
        if(PoSTable != null) {
            convertPoS(transduced);
        }

        if(morphologicalFeatureTable != null) {
            convertFeatures(transduced);
        }

        treeToFile(outFile, transduced);
    }

    private static void convertFeatures(Root transduced) {
        transduced.getDescendants().forEach(node -> {
            String[] featureList = node.getFeats().split("\\|");
            String newFeatureList = "";
            for(String feature: featureList) {
                String newFeature = morphologicalFeatureTable.get(feature);
                if(newFeature != null) {
                    newFeatureList += "|" + newFeature;
                }
            }
            if(newFeatureList.length() > 0) {
                newFeatureList = newFeatureList.substring(1);
            } else {
                newFeatureList = "_";
            }
            featureList = newFeatureList.split("\\|");
            Arrays.sort(featureList, String::compareTo);
            node.setFeats(String.join("|", featureList));
        });
    }

    private static void convertPoS(Root transduced) {
        transduced.getDescendants().forEach(node -> {
            String newpos = PoSTable.get(node.getXpos());
            if(newpos != null) {
                if(PoSTableExceptions.containsKey(node.getXpos() + ":" + node.getDeprel())) {
                    node.setUpos(PoSTableExceptions.get(node.getXpos() + ":" + node.getDeprel()));
                } else {
                    node.setUpos(PoSTable.get(node.getXpos()));
                }
            }
        });
    }

    private static void testMain(Namespace ns) {
        logger.info("Testing");
        String compDir = ns.getString("expected_dir");
        String genDir = ns.getString("actual_dir");
        String origDir = ns.getString("original_dir");

        int punctuationNodes = 0;
        int ignoredNodes = 0;
        int notConvertedNodes = 0;
        int incorrectNodes = 0;
        int subtypesIncorrectNodes = 0;
        int correctNodes = 0;
        int completeMatch = 0;

        FilenameFilter conllFilter = (dir, name) -> name.toLowerCase().endsWith(".conll");

        File[] files = new File(genDir).listFiles(conllFilter);
        Arrays.sort(files);

        for (File file : files) {
            logger.info(String.format("Testing file %s", file.getName()));
            logger.debug("reading generated");
            Root generated = fileToTree(file);
            logger.debug("reading expected");
            Root expected = fileToTree(new File(compDir, file.getName()));
            Root original = fileToTree(new File(origDir, file.getName()));
            TreeComparator tc = new TreeComparator(expected, generated);
            tc.compare();
            if (tc.matches()) {
                completeMatch += 1;
                logger.info(String.format("%s equal", file.getName()));
            } else {
                logger.info(String.format("%s incorrect: %s, subtypes incorrect: %s, unconverted: %s", file.getName(), intlistToString(tc.getDidntMatch()), intlistToString(tc.getSubtypesDidntMatch()), intlistToString(tc.getNotConverted())));
            }
            punctuationNodes += tc.getPunctuation().size();
            ignoredNodes += tc.getIgnored().size();
            notConvertedNodes += tc.getNotConverted().size();
            incorrectNodes += tc.getDidntMatch().size();
            subtypesIncorrectNodes += tc.getSubtypesDidntMatch().size();
            correctNodes += tc.getDidMatch().size();
        }

        logger.info(String.format("%d nodes punctuation.", punctuationNodes));
        logger.info(String.format("%d nodes ignored (not annotated in 'expected').", ignoredNodes));
        logger.info(String.format("%d nodes not converted by transducer.", notConvertedNodes));
        logger.info(String.format("%d nodes not converted correctly.", incorrectNodes));
        logger.info(String.format("%d nodes with incorrect subtypes.", subtypesIncorrectNodes));
        logger.info(String.format("%d nodes converted correctly.", correctNodes));
        int total = punctuationNodes + ignoredNodes + notConvertedNodes + incorrectNodes + subtypesIncorrectNodes + correctNodes;
        logger.info(String.format("%d nodes total", total));
        logger.info(String.format("%d/%d sentences correct", completeMatch, files.length));

        PrecisionStats ps = new PrecisionStats();
        for (File file : files) {
            Root generated = fileToTree(file);
            Root expected = fileToTree(new File(compDir, file.getName()));
            Root original = fileToTree(new File(origDir, file.getName()));
            ps.accumulate(original, expected, generated);
        }
        String x = ps.breakdownAsString();
        logger.info("\n" + x);

    }

    private static void checkCoverageMain(Namespace ns) {
        String genDir = ns.getString("dir");
        String origDir = ns.getString("original_dir");


        File[] files = new File(genDir).listFiles();
        Arrays.sort(files);

        CoverageChecker cc = new CoverageChecker();

        for (File file : files) {
            Root r = fileToTree(file);
            Root orig = fileToTree(new File(origDir + "/" + file.getName()));
            cc.checkTree(orig, r);
        }

        int correctNodes = cc.getTotalConvertedCount();
        int puncuationNodes = cc.getTotalPunctuationCount();
        int blockers = cc.getTotalBlockerCount();
        int indirectly = cc.getTotalIndirectlyNotConvertedCount();

        logger.info(String.format("%d trees total", cc.getTotalTreeCount()));
        logger.info(String.format("%d fully converted Trees", cc.getConvertedTreeCount()));

        logger.info(String.format("%d nodes converted correctly.", correctNodes));
        logger.info(String.format("%d nodes punctuation.", puncuationNodes));
        logger.info(String.format("%d nodes blockers.", blockers));
        logger.info(String.format("%d nodes not converted follow up.", indirectly));

        // Perform relevant calculations.
        int totalWithoutPunct = correctNodes + blockers + indirectly;
        double correctPercentage = (double)correctNodes / (double)totalWithoutPunct;
        double blockersPercentage = (double)blockers / (double)totalWithoutPunct;
        double indirectPercentage = (double)indirectly / (double)totalWithoutPunct;

        logger.info(String.format("%d total without punctuation", totalWithoutPunct));
        logger.info(String.format("%.5f correct", correctPercentage));
        logger.info(String.format("%.5f blockers", blockersPercentage));
        logger.info(String.format("%.5f indirect", indirectPercentage));

        logger.info("\n" + cc.getTableAsString());
        logger.info("\n" + cc.getBlockerStatsAsString());

    }

    /**
     * starts the graphical tool for developing and checking rule files.
     * This will not convert the treebank. If the output directory is already populated
     * (the treebank has been converted), the tool will show the coverage stats.
     * Otherwise, it will only list the unconverted sentences.
     * @param ns
     */
    private static void sessionMain(Namespace ns) {
        String genDir = ns.getString("output_dir");
        String inDir = ns.getString("input_dir");

        String posPath = ns.getString("pos_table");
        if(posPath != null && !posPath.equals("")) {
            generatePoSTable(posPath);
        }
        String featPath = ns.getString("feat_table");
        if(featPath != null && !featPath.equals("")) {
            generateFeatTable(featPath);
        }


        CoverageChecker cc = new CoverageChecker();
        SessionGUIController gui = new SessionGUIController(inDir, genDir);

        Integer cts = ns.getInt("treecount");

        if(cts == null || cts.equals(0)) {
            cts = Integer.MAX_VALUE;
        }

        conversionTreeSize = cts;

        String[] fileNames = TreeReaderWriter.getFileNames(new File(inDir));

        int i = 0;

        for (String paths: fileNames) {
            File file = new File(inDir, paths);
            i++;
            if(i > conversionTreeSize) {
                break;
            }
            try {
                Root r = fileToTree(file);
                File genFile = new File(genDir + "/" + file.getName());
                Root gen = genFile.exists() ? fileToTree(genFile) : r;
                TreeConversionStats tStats = cc.checkTree(r, gen);

                gui.add(genFile.exists() ? genFile : file, file, tStats, genFile.exists());
            } catch (Exception e) {
                logger.log(Priority.WARN, String.format("cannot parse file %s, skipping...", file.getPath()));
            }
        }

        int blockers = cc.getTotalBlockerCount();

        int convertedSentences = cc.getConvertedTreeCount();
        int sentencesCount = cc.getTotalTreeCount();

        // Perform relevant calculations.
        double failedSentencePercentage = 1.0 - (double) convertedSentences / (double) sentencesCount;

        gui.setPercentage(failedSentencePercentage * 100d);
        Map<String, Integer> blockersIndividual = cc.getBlockersIndividual();
        for(String blocker: blockersIndividual.keySet()) {
            gui.setPercentage(blocker, 100d * (double) blockersIndividual.get(blocker) / (double) blockers);
        }

        gui.setVerbose(ns.getBoolean("verbose"));
        if(ns.getBoolean("verbose"))
            System.out.println("in verbose mode!!");

        gui.initWindow();

        String transducerPath = ns.getString("transducer");

        if(transducerPath != null && !transducerPath.equals("")) {
            transducerPath = transducerPath.replace("[", "");
            transducerPath = transducerPath.replace("]", "");
            gui.setTransducerPath(transducerPath);
        }

        String validationPath = ns.getString("validation");

        if(validationPath != null && !validationPath.equals("")) {
            validationPath = validationPath.replace("[", "");
            validationPath = validationPath.replace("]", "");
            gui.setValidationPath(validationPath);
        }

        gui.sortTree();
    }

    private static void generatePoSTable(String posPath) {
        posPath = posPath.replace("[", "").replace("]", "");
        PoSTable = new HashMap<>();
        PoSTableExceptions = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(
                    posPath));
            String line = null;
            line = reader.readLine();
            while (line != null) {
                String[] split = line.split(" - ");
                PoSTable.put(split[0], split[1]);
                for(int i = 2; i < split.length; i++) {
                    String[] exceptionSplit = split[i].split("->");
                    PoSTableExceptions.put(split[0] + ":" + exceptionSplit[0], exceptionSplit[1]);
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void generateFeatTable(String featPath) {
        featPath = featPath.replace("[", "").replace("]", "");
        morphologicalFeatureTable = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(
                    featPath));
            String line = null;
            line = reader.readLine();
            while (line != null) {
                String[] split = line.split(" - ");
                morphologicalFeatureTable.put(split[0], split[1]);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void listMain(Namespace ns) {
        String srcDir = ns.getString("src_dir");
        String genDir = ns.getString("gen_dir");
        boolean onlyIncomplete = ns.getBoolean("incomplete");
        boolean showBlockers = ns.getBoolean("blockers");

        File[] files = new File(genDir).listFiles();
        Arrays.sort(files);

        for (File file : files) {
            Root gen = fileToTree(file);
            Root orig = fileToTree(new File(srcDir + "/" + file.getName()));
            TreeConversionStats tcStats = new TreeConversionStats(orig, gen);
            tcStats.check();
            if (!onlyIncomplete || !tcStats.isTreeFullyConverted()) {
                StringBuilder sb = new StringBuilder();
                sb.append(file.getName());
                if (showBlockers) {
                    String blockers = tcStats.getBlockerNodes().stream().map(Node::getDeprel).collect(Collectors.joining(", "));
                    sb.append(" " + blockers);
                }
                System.out.println(sb.toString());
            }
        }
    }

    private static void searchMain(Namespace ns) {
        String dir = ns.getString("dir");
        String searchExpression = ns.getString("expr");

        // parse search expression
        ANTLRInputStream input = new ANTLRInputStream(searchExpression);
        TransducerLexer lexer = new TransducerLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        TransducerParser parser = new TransducerParser(tokens);
        TransducerParser.MatchTreeContext context =  parser.matchTree(new HashMap<>());
        Tree queryTree = context.tree;
        //infer missing structure by creating a dummy rule ... really dirty hack, sorry!
        Rule dummyRule = new Rule(queryTree, generateDummyReplacementNode(queryTree), null, "");

        File[] files = new File(dir).listFiles();
        Arrays.sort(files);

        NodeClassifier defaultNodeClassifier = new NodeClassifier();

        for (File file : files) {
            Root tree = fileToTree(file);
            ConversionState convState = new ConversionState(tree, defaultNodeClassifier);

            for (DepTreeFrontierNode frontierNode : convState.getFrontier()) {
                Binding match = Matcher.getBinding(queryTree.frontierNode, frontierNode);
                if (match != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(file.getName());
                    sb.append(":");
                    for (String key : queryTree.getUsedNames()) {
                        Node n = match.singles.get(key);
                        if (n != null) {
                            sb.append("[" + key + ":" + n.getDeprel() + "]");
                        }
                    }
                    System.out.println(sb.toString());
                }
            }
        }
    }

    public static ReplacementNode generateDummyReplacementNode(Tree matchTree) {
        List<String> usedNames = matchTree.getUsedNames();
        ReplacementNode dummyParent = new ReplacementNode();
        dummyParent.setName(usedNames.stream().collect(Collectors.joining()));
        for (String name : usedNames) {
            ReplacementNode rNode = new ReplacementNode();
            rNode.setName(name);
            rNode.setParent(dummyParent);
            dummyParent.addChild(rNode);
        }
        return dummyParent;
    }

    private static void showTreeMain(Namespace ns) throws IOException {
        String conllFilePath = ns.getString("input_file");
        String transducerPath = ns.getString("transducer_file");

        Root tree = fileToTree(new File(conllFilePath));
        Transducer transducer = null;

        if (transducerPath != null) {
            transducer = pathToTransducer(transducerPath);
        }

        MainWindowController controller = new MainWindowController();
        controller.initWindow();
        controller.setTitle(new File(conllFilePath).getCanonicalPath() + " - TrUDucer");
        controller.setTree(tree, transducer.getNodeClassifier());
        controller.setTransducer(transducer);
    }

    private static Transducer pathToTransducer(String path) throws IOException {
        ANTLRFileStream input = new ANTLRFileStream(path);
        TransducerLexer lexer = new TransducerLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        TransducerParser parser = new TransducerParser(tokens);

        TransducerParser.TransducerContext tree = parser.transducer();
        return tree.t;
    }

    private static Root pathToTree(String path) {
        return fileToTree(new File(path));
    }

    private static String intlistToString(List<Integer> intlist) {
        if (intlist.size() == 0) {
            return "[]";
        } else if (intlist.size() == 1) {
            return String.format("[%d]", intlist.get(0));
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(intlist.get(0));
            for (Integer i : intlist.subList(1, intlist.size())) {
                sb.append(", ");
                sb.append(i);
            }
            sb.append("]");
            return sb.toString();
        }
    }

}
