package nats.truducer.groovy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class GroovyDictionary extends HashMap<String, List<String>> {

    @Override
    public List<String> get(Object key) {
        if (containsKey(key)) {
            return super.get(key);
        }
        List<String> newList = new ArrayList<>();
        if(key instanceof String)
            put((String) key, newList);
        return newList;
    }

    public void readFromFile(String fileName) {
        clear();

        try {
            Properties p = new Properties();
            p.load(new FileInputStream(fileName));
            p.forEach((key, value) -> {
                List<String> l = new ArrayList<>();
                Collections.addAll(l, ((String) value).split(","));
                put((String) key, l);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToFile(String fileName) {
        try {
            Properties p = new Properties();
            forEach((key, list) ->  {
                if(list.size() > 0)
                    p.put(key, list.stream().distinct().reduce((a, b) -> a + "," + b).orElse(""));
            });
            p.store(new FileOutputStream(fileName), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
