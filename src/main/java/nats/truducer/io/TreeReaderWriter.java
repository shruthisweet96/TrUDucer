package nats.truducer.io;

import com.google.common.base.CharMatcher;
import cz.ufal.udapi.core.Bundle;
import cz.ufal.udapi.core.Document;
import cz.ufal.udapi.core.Node;
import cz.ufal.udapi.core.Root;
import cz.ufal.udapi.core.impl.DefaultDocument;
import cz.ufal.udapi.core.io.impl.CoNLLUReader;
import cz.ufal.udapi.core.io.impl.CoNLLUWriter;
import nats.truducer.data.NodeClassifier;
import nats.truducer.data.Transducer;
import nats.truducer.io.ruleparsing.TransducerLexer;
import nats.truducer.io.ruleparsing.TransducerParser;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class for reading and writing trees to files using the CoNLLReader and CoNLLWriter classes.
 */

public class TreeReaderWriter {

    public static final NodeClassifier defaultNodeClassifier = new NodeClassifier();
    private static Logger logger = Logger.getLogger(TreeReaderWriter.class);

    /**
     * Generates a Transducer Object from a given ruleset in rule syntax
     * @param path Path to rule file
     * @return Transducer generated from rule file
     * @throws IOException
     */
    public static Transducer pathToTransducer(String path) throws IOException {
        ANTLRFileStream input = new ANTLRFileStream(path);
        TransducerLexer lexer = new TransducerLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        TransducerParser parser = new TransducerParser(tokens);

        TransducerParser.TransducerContext tree = parser.transducer();
        return tree.t;
    }

    /**
     * loads a CoNLLU-formatted tree file and returns it as udapi-java tree
     * @param file
     * @return
     */
    public static Root fileToTree(File file) {
        Document doc = new CoNLLUReader(file).readDocument();
        // logger.debug(String.format("Bundles: %d", doc.getBundles().size()));
        // logger.debug(String.format("Trees: %d", doc.getBundles().get(0).getTrees().size()));

        Root tree = doc.getBundles().get(0).getTrees().get(0);
        tree.setSentId(file.getName().substring(0, file.getName().lastIndexOf('.')));


        StringBuilder sb = new StringBuilder();
        tree.getDescendants().forEach(s -> {sb.append(s.getForm()); sb.append(' ');});
        if(tree.getComments().size() == 0)
            tree.addComment(" text = " + sb.toString().trim());

        return tree;
    }

    /**
     * Extracts all the Sentences in a CoNLL09 or CoNLLU file and stores them into seperate files containing one
     * Sentence in CoNLLU-format each
     * @param file File containing all the sentences
     * @param outDir Target directory, where the single-sentence CoNLLU files will be put
     * @throws IOException
     */
    public static void extractTrees(File file, File outDir) throws IOException {
        File tempFile = new File(outDir, FilenameUtils.getBaseName(file.getName()) + "_temp.CoNNLU");

        // check, if file is of format CoNLL09 or CoNLLU:
        Scanner scanner = new Scanner(file);
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(line.trim().length() > 0) {
                scanner.close();
                if(line.trim().split("\t").length == 15) {
                    // CoNLL09:
                    convertCoNLL09ToCoNLLU(file, tempFile);
                    file = tempFile;
                }
                break;
            }
        }

        Document doc = new CoNLLUReader(file).readDocument();
        logger.info(String.format("Bundles: %d", doc.getBundles().size()));
        logger.info(String.format("Trees: %d", doc.getBundles().get(0).getTrees().size()));

        int toIndex = doc.getBundles().size();

        if(toIndex > 1000) {
            String r = JOptionPane.showInputDialog("The given treebank is quite big. Please specify, how many sentences you want to extract.", toIndex);
            try {
                toIndex = Integer.parseInt(r);
            } catch (NumberFormatException e) {
                logger.error("invalid input: " + r);
            }
        }

        List<Root> trees = doc.getBundles().subList(0, toIndex).stream().map(b -> b.getTrees().get(0)).collect(Collectors.toList());

        if(tempFile.exists()) {
            tempFile.delete();
        }

        int i = 0;
        for(Root r: trees) {
            treeToFile(new File(outDir.toString(), "s_" + i + ".CoNLL"), r);
            i++;
        }

    }

    /**
     * packs all the CoNLLU-files in a directory into one big CoNLLU-file
     * @param file target file
     * @param inDir directory containing CoNLLU-files to be merged into one
     */
    public static void condenseTrees(File file, File inDir) throws IOException {
        if(!file.exists())
            file.createNewFile();
        FileOutputStream stream = new FileOutputStream(file, false);

        File[] inFiles = inDir.listFiles((d, name) -> name.toLowerCase().endsWith(".conll"));
        if(inFiles != null) {
            Arrays.sort(inFiles, (a, b) -> {
                int aName = Integer.parseInt("0" + CharMatcher.digit().retainFrom(a.getName()));
                int bName = Integer.parseInt("0" + CharMatcher.digit().retainFrom(b.getName()));
                return Integer.compare(aName, bName);
            });

            fileLoop:
            for(File f: inFiles) {

                Root tree = fileToTree(f);

                for(Node n: tree.getDescendants()) {
                    // TODO this is not valid for all treebanks!
                    // This will only add trees with all lower case
                    // Actually, there might be trees where in the original tree there are lower case
                    // deprels, in the target treebank there might be upper case letters or the resulting tree
                    // is supposed to share some dependencies with the original tree (like the ROOT nodes!!)
                    if(!(n.getDeprel().equals("ROOT") || n.getDeprel().equals(n.getDeprel().toLowerCase())))
                        continue fileLoop;
                }

                Files.copy(f.toPath(), stream);
            }
        }
        stream.flush();
        stream.close();
    }

    public static String[] getFileNames(File dir) {
        String[] fileNames = dir.list();
        if(fileNames == null) {
            logger.warn("no files in directory.");
            return new String[0];
        }
        Arrays.sort(fileNames, (a, b) -> {
            int aName = Integer.parseInt("0" + CharMatcher.digit().retainFrom(a));
            int bName = Integer.parseInt("0" + CharMatcher.digit().retainFrom(b));
            return Integer.compare(aName, bName);
        });

        return fileNames;
    }

    private static void convertCoNLL09ToCoNLLU(File srcFile, File dstFile) throws IOException {
        FileWriter writer = new FileWriter(dstFile);

        Pattern sentenceNumber = Pattern.compile("^[\\d]+_");

        Stream<String> lines = Files.lines(srcFile.toPath());
        lines = lines.map(s -> {
            if(s.trim().length() > 0) {
                String[] sp = sentenceNumber.matcher(s).replaceAll("").split("\t");

                String[] nsp = new String[10];
                System.arraycopy(sp, 0, nsp, 0, 5);
                nsp[5] = sp[6];
                nsp[6] = sp[8];
                nsp[7] = sp[10];
                System.arraycopy(sp, 11, nsp, 8, 2);
                if (nsp[4].equals("$."))
                nsp[7] = "PUNCT";
                if (nsp[4].equals("$,"))
                nsp[7] = "PUNCT";
                if (nsp[4].equals("$("))
                nsp[7] = "PUNCT";
                if (nsp[6].equals("0"))
                nsp[7] = "ROOT";

                return String.join("\t", nsp) + "\n";
            }
            return "\n";
        });
        lines.forEachOrdered(s -> {
            try {
                writer.write(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        writer.close();
    }

    /**
     * stores a udapi-tree to a CoNLLU-formated file
     * @param file
     * @param tree
     */
    public static void treeToFile(File file, Root tree) {
        Document outDoc = new DefaultDocument();
        outDoc.createBundle().addTree(tree);

        writeDoc(outDoc, file);
    }

    /**
     * The CoNNLUWriter seems to not care about ordering of the words/nodes.
     * The CoNNLUReader very much does so!
     * As long as the reader is not fixed, we need to be careful about ordering
     * our output. This should help...
     *
     * @param outDoc
     * @param outFile
     */
    private static void writeDoc(Document outDoc, File outFile) {
        new CoNLLUWriter().writeDocument(outDoc, outFile.toPath());

        // tree ordering should be fixed!
        /*
        try {
            BufferedReader r = new BufferedReader(new FileReader(outFile));
            String s;
            List<String> strings = new ArrayList<>();
            while ((s = r.readLine()) != null) {
                strings.add(s);
            }
            r.close();
            strings.sort((a, b) -> {
                if(a.startsWith("#") || b.length() < 1)
                    return Integer.compare(1, 0);
                if(b.startsWith("#") || a.length() < 1)
                    return Integer.compare(0, 1);
                int first = Integer.parseInt(a.split("\t")[0]);
                int second = Integer.parseInt(b.split("\t")[0]);
                return Integer.compare(first, second);
            });

            FileWriter w = new FileWriter(outFile);

            // weiss nicht, ob das die schoenste Implementation in Java ist :)
            w.write(strings.stream().reduce("", (a, b) -> a + "\n" + b));
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
    }

}
