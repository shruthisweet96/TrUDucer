package nats.truducer.gui;

import cz.ufal.udapi.core.Root;
import nats.truducer.data.NodeClassifier;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;

public class SessionGUI {

    public final JFrame frame;
    public final JTree tree;
    public final TreeModel treeModel;
    public final DeptreeViewPane deptreeViewPane;

    public final JScrollPane scrollPane;

    public final JCheckBoxMenuItem reloadTransducer;
    public final JMenuItem setTransducer;

    public final JMenuItem getOriginalTree;
    public final JMenuItem showCoNLL;

    public final JMenuItem setOutDir;
    public final JMenuItem setInDir;
    public final JMenuItem setValidationDir;
    public final JMenuItem loadInputFile;
    public final JMenuItem storeOutputFile;

    public final JCheckBoxMenuItem rememberRules;
    public final JMenuItem convert;
    public final JMenuItem validate;
    public final JMenuItem showStats;

    public SessionGUI(SessionGUIController controller, DefaultMutableTreeNode root) {
        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();

        JMenu transducerMenu = new JMenu("Transducer");
        menuBar.add(transducerMenu);

        reloadTransducer = new JCheckBoxMenuItem("automatically reload Transducer");
        reloadTransducer.setState(true);
        transducerMenu.add(reloadTransducer);

        setTransducer = new JMenuItem("set Transducer");
        transducerMenu.add(setTransducer);
        setTransducer.addActionListener(controller);

        JMenu treebankMenu = new JMenu("File");
        menuBar.add(treebankMenu);

        setOutDir = new JMenuItem("set output directory");
        treebankMenu.add(setOutDir);
        setOutDir.addActionListener(controller);

        setInDir = new JMenuItem("set input directory");
        treebankMenu.add(setInDir);
        setInDir.addActionListener(controller);

        setValidationDir = new JMenuItem("set validation directory");
        treebankMenu.add(setValidationDir);
        setValidationDir.addActionListener(controller);

        loadInputFile = new JMenuItem("extract conll treebank");
        treebankMenu.add(loadInputFile);
        loadInputFile.addActionListener(controller);

        storeOutputFile = new JMenuItem("condense converted treebank");
        treebankMenu.add(storeOutputFile);
        storeOutputFile.addActionListener(controller);


        JMenu treeMenu = new JMenu("Tree");
        menuBar.add(treeMenu);

        getOriginalTree = new JMenuItem("load original tree");
        treeMenu.add(getOriginalTree);
        getOriginalTree.addActionListener(controller);

        showCoNLL = new JMenuItem("show as CoNLL text");
        treeMenu.add(showCoNLL);
        showCoNLL.addActionListener(controller);

        JMenu conversionMenu = new JMenu("Conversion");
        menuBar.add(conversionMenu);

        rememberRules = new JCheckBoxMenuItem("remember rules used");
        rememberRules.setState(true);
        conversionMenu.add(rememberRules);

        convert = new JMenuItem("convert all");
        convert.addActionListener(controller);
        conversionMenu.add(convert);

        validate = new JMenuItem("validate");
        validate.addActionListener(controller);
        conversionMenu.add(validate);

        showStats = new JMenuItem("show rule statistics");
        showStats.addActionListener(controller);
        conversionMenu.add(showStats);

        frame.setJMenuBar(menuBar);

        Container pane = frame.getContentPane();
        pane.setLayout(new BorderLayout());

        treeModel = new DefaultTreeModel(root);

        tree = new JTree(treeModel);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(controller);
        tree.addMouseListener(controller);
        tree.setRootVisible(false);
        tree.setCellRenderer(new ColorCellRenderer());
        // tree.setFont(new Font(Font.MONOSPACED, Font.PLAIN, tree.getFont().getSize()));

        scrollPane = new JScrollPane(tree);
        scrollPane.setMaximumSize(new Dimension(400, 1800));
        scrollPane.setPreferredSize(new Dimension(400, 600));

        pane.add(scrollPane, BorderLayout.WEST);

        // DepTree View
        deptreeViewPane = new DeptreeViewPane();

        pane.add(deptreeViewPane.getPanel(), BorderLayout.CENTER);

        frame.pack();
        frame.setVisible(true);
    }

    public void setTree(Root tree, NodeClassifier nodeClassifier) {
        deptreeViewPane.setTree(tree, nodeClassifier);
    }
}

