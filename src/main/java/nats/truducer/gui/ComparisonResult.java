package nats.truducer.gui;

import nats.truducer.deprel.TreeComparator;
import nats.truducer.deprel.TreeConversionStats;

import java.io.File;

public class ComparisonResult extends ConversionResult {
    private File goldFile;

    public ComparisonResult(File file, File srcFile, File goldFile, TreeConversionStats stats, TreeComparator tc) {
        super(file, srcFile, stats, true);
        this.goldFile = goldFile;
        successful = tc.matches();
    }

    public File getGoldFile() {
        return this.goldFile;
    }
}
