package nats.truducer.gui;

import cz.ufal.udapi.core.Node;
import cz.ufal.udapi.core.Root;
import cz.ufal.udapi.core.impl.DefaultDocument;
import cz.ufal.udapi.core.impl.DefaultRoot;
import nats.truducer.data.*;
import nats.truducer.deprel.TreeComparator;
import nats.truducer.deprel.TreeConversionStats;
import nats.truducer.io.CoNLLMatcher;
import nats.truducer.io.TreeReaderWriter;
import nats.truducer.io.ruleparsing.TransducerLexer;
import nats.truducer.io.ruleparsing.TransducerParser;
import nats.truducer.startup.Main;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.List;

import static nats.truducer.io.TreeReaderWriter.defaultNodeClassifier;
import static nats.truducer.io.TreeReaderWriter.fileToTree;
import static nats.truducer.io.TreeReaderWriter.pathToTransducer;

/**
 * implementation of the GUI logic.
 */
public class SessionGUIController implements TreeSelectionListener, ActionListener, MouseListener {
    private static final Logger logger = Logger.getLogger("session");

    private SessionGUI gui;

    /**
     * The converted files are displayed in a file-like structure.
     * "root" is the hidden tree root with four subcategories.
     */
    private DefaultMutableTreeNode root;

    /**
     * In catRoot, not converted sentences are grouped by the dependency relation of the blocking frontier node.
     * In ruleRoot, all sentences are put in the subtree of each rule that was successfully applied in the conversion process.
     *   (there can also be not converted trees in those subtrees, they will be highlighted in red)
     * In allRoot, all trees are put regardless of their conversion process and ordered by ID
     *   (arithmetically, not alphabetically)
     * In validationRoot, the validation can be checked. Converted trees that differ from validation tree are highlighted in red.
     */
    private DefaultMutableTreeNode catRoot;
    private DefaultMutableTreeNode ruleRoot;
    private DefaultMutableTreeNode allRoot;
    private DefaultMutableTreeNode validationRoot;

    private HashMap<String, DefaultMutableTreeNode> frontiers;
    private HashMap<Rule, DefaultMutableTreeNode> rules;
    private HashMap<Rule, Integer> ruleUseCounts;

    private String inPath;
    private String outPath;
    private String validationPath;
    private String transducerPath;
    private boolean verbose;

    public SessionGUIController(String inPath, String outPath) {
        createRootTreeNode();
        frontiers = new HashMap<>();
        rules = new HashMap<>();
        ruleUseCounts = new HashMap<>();
        this.inPath = inPath;
        this.outPath = outPath;
        this.validationPath = ".";
        this.transducerPath = "sample_rules_hdt.tud";
        this.verbose = false;
    }

    public void initWindow() {
        gui = new SessionGUI(this, root);
    }

    /**
     * adds a tree to the viewer
     * @param file converted tree
     * @param srcFile source tree
     * @param stats what rules where applied, what are blocking frontier nodes etc.
     * @param converted is conversion successful?
     */
    public void add(File file, File srcFile, TreeConversionStats stats, boolean converted) {
        ConversionResult conversionResult = new ConversionResult(file, srcFile, stats, converted);

        allRoot.add(new DefaultMutableTreeNode(conversionResult));

        if(stats.getRulesUsed() != null) {
            stats.getRulesUsed().forEach(r -> {
                if(r == null)
                    return;
                DefaultMutableTreeNode rule;
                if(!rules.containsKey(r)) {
                    rule = new DefaultMutableTreeNode(r);
                    rules.put(r, rule);
                    ruleRoot.add(rule);
                    ruleUseCounts.put(r, 1);
                } else {
                    rule = rules.get(r);
                    ruleUseCounts.put(r, ruleUseCounts.get(r) + 1);
                }
                rule.add(new DefaultMutableTreeNode(conversionResult));
            });
        }

        if(!stats.isTreeFullyConverted()) {
            stats.getBlockerNodes().stream().map(Node::getDeprel).distinct().forEach(s -> {
                DefaultMutableTreeNode category;
                if(!frontiers.containsKey(s)) {
                    category = new DefaultMutableTreeNode(s);
                    frontiers.put(s, category);
                    catRoot.add(category);
                } else {
                    category = frontiers.get(s);
                }
                category.add(new DefaultMutableTreeNode(conversionResult));
            });
        }
    }

    /**
     * initializes all the different categories.
     */
    private void createRootTreeNode() {
        root = new DefaultMutableTreeNode("all");
        catRoot = new DefaultMutableTreeNode("failed conversions");
        ruleRoot = new DefaultMutableTreeNode("conversions by rule");
        allRoot = new DefaultMutableTreeNode("all");
        validationRoot = new DefaultMutableTreeNode("validation set");
        root.add(allRoot);
        root.add(catRoot);
        root.add(ruleRoot);
        root.add(validationRoot);
    }

    public void setPercentage(double percentage) {
        catRoot.setUserObject(String.format("%s overall: %2.4f%%", catRoot.getUserObject().toString(), percentage));
    }

    public void setPercentage(String s, double percentage) {
        DefaultMutableTreeNode category = frontiers.get(s);
        if(category != null) {
            category.setUserObject(String.format("%s: %2.4f%%", category.getUserObject().toString(), percentage));
        }
    }

    /**
     * if a tree is selected, it should be rendered in the tree viewer
     * @param e
     */
    @Override
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) gui.tree.getLastSelectedPathComponent();
        if(node == null) {
            // nothing selected
            return;
        }

        if(!node.isLeaf() || !(node.getUserObject() instanceof ConversionResult)) {
            // we can't render categories
            return;
        }

        ConversionResult cr = (ConversionResult) node.getUserObject();
        gui.setTree(fileToTree(cr.getFile()), defaultNodeClassifier);
    }

    /**
     * menu items
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == gui.setTransducer) {
            JFileChooser chooser = new JFileChooser(transducerPath);
            int returnVal = chooser.showOpenDialog(chooser);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                try {
                    gui.deptreeViewPane.setTransducer(pathToTransducer(f.getPath()));
                } catch (IOException e1) {
                    // cannot set transducer!
                    logger.error("cannot set transducer!");
                    logger.error(e1);
                }
                this.transducerPath = f.getPath();
            }
        }

        else if(e.getSource() == gui.getOriginalTree) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) gui.tree.getLastSelectedPathComponent();
            if(node == null) {
                // nothing selected
                return;
            }

            if(!node.isLeaf() || !(node.getUserObject() instanceof ConversionResult)) {
                // we can't render categories
                return;
            }

            ConversionResult cr = (ConversionResult) node.getUserObject();
            gui.setTree(fileToTree(cr.getSrcFile()), defaultNodeClassifier);
        }

        else if(e.getSource() == gui.showCoNLL) {
            gui.deptreeViewPane.showConLL(gui.frame);
        }

        else if(e.getSource() == gui.convert) {

            CursorManager.waitForTask(gui.frame, () -> {
                if(gui.reloadTransducer.getState()) {
                    try {
                        gui.deptreeViewPane.setTransducer(pathToTransducer(transducerPath));
                    } catch (IOException e1) {
                        logger.error("cannot set transducer!");
                        logger.error(e1);
                    }
                }

                createRootTreeNode();
                frontiers.clear();
                rules.clear();
                Main.convertDir(gui.deptreeViewPane.getTransducer(), inPath, outPath, gui.rememberRules.getState(), this);
                ((DefaultTreeModel)gui.treeModel).setRoot(root);
                ((DefaultTreeModel)gui.treeModel).reload(root);
            });
        } else if(e.getSource() == gui.showStats) {
            StringBuilder result = new StringBuilder();
            result.append("total rule count: " + ruleUseCounts.size());

            for(Rule r: ruleUseCounts.keySet()) {
                result.append(" " + r.toString() + ": " + ruleUseCounts.get(r));
            }
            new TextBoxPopup(gui.frame, result.toString());
        }

        else if(e.getSource() == gui.validate) {
            CursorManager.waitForTask(gui.frame, this::validate);
        }

        else if(e.getSource() == gui.setOutDir) {
            JFileChooser chooser = new JFileChooser(outPath);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = chooser.showOpenDialog(chooser);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                this.outPath = f.getPath();
            }
        }

        else if(e.getSource() == gui.setInDir) {
            JFileChooser chooser = new JFileChooser(inPath);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = chooser.showOpenDialog(chooser);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                this.inPath = f.getPath();

                CursorManager.waitForTask(gui.frame, () -> {
                    if(gui.reloadTransducer.getState()) {
                        try {
                            gui.deptreeViewPane.setTransducer(pathToTransducer(transducerPath));
                        } catch (IOException e1) {
                            logger.error("cannot set transducer");
                            logger.error(e1);
                        }
                    }

                    createRootTreeNode();
                    frontiers.clear();
                    rules.clear();
                    Main.convertDir(gui.deptreeViewPane.getTransducer(), inPath, outPath, gui.rememberRules.getState(), this);
                    ((DefaultTreeModel)gui.treeModel).setRoot(root);
                    ((DefaultTreeModel)gui.treeModel).reload(root);
                });
            }
        }

        else if(e.getSource() == gui.setValidationDir) {
            JFileChooser chooser = new JFileChooser(validationPath);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = chooser.showOpenDialog(chooser);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                this.validationPath = f.getPath();
            }
        }

        else if(e.getSource() == gui.loadInputFile) {
            int dialogResult = JOptionPane.showConfirmDialog (
                    null,
                    "This might overwrite files source folder! Continue?",
                    "Warning",
                    JOptionPane.YES_NO_OPTION);
            if(dialogResult != JOptionPane.YES_OPTION) {
                return;
            }

            JFileChooser chooser = new JFileChooser(".");
            int returnVal = chooser.showOpenDialog(chooser);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();

                CursorManager.waitForTask(gui.frame, () -> {

                    // extract trees from given file
                    try {
                        TreeReaderWriter.extractTrees(f, new File(inPath));
                    } catch (IOException e1) {
                        logger.error("error in extracting trees!");
                        logger.error(e1);
                    }

                    if(gui.reloadTransducer.getState()) {
                        try {
                            gui.deptreeViewPane.setTransducer(pathToTransducer(transducerPath));
                        } catch (IOException e1) {
                            logger.error("cannot set transducer!");
                            logger.error(e1);
                        }
                    }

                    createRootTreeNode();
                    frontiers.clear();
                    rules.clear();
                    Main.convertDir(gui.deptreeViewPane.getTransducer(), inPath, outPath, gui.rememberRules.getState(), this);
                    ((DefaultTreeModel)gui.treeModel).setRoot(root);
                    ((DefaultTreeModel)gui.treeModel).reload(root);

                });
            }
        } else if (e.getSource() == gui.storeOutputFile) {
            JFileChooser chooser = new JFileChooser(".");
            int returnVal = chooser.showOpenDialog(chooser);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();

                // if the file exists, better ask if you really want to loose it's content!
                if(f.exists()) {
                    int dialogResult = JOptionPane.showConfirmDialog (
                            null,
                            "This will overwrite " + f.getName() + "! Continue?",
                            "Warning",
                            JOptionPane.YES_NO_OPTION);
                    if(dialogResult != JOptionPane.YES_OPTION) {
                        return;
                    }
                }

                CursorManager.waitForTask(gui.frame, () -> {
                    // extract trees from given file
                    try {
                        TreeReaderWriter.condenseTrees(f, new File(outPath));
                    } catch (IOException e1) {
                        logger.error("cannot condense trees!");
                        logger.error(e1);
                    }
                });
            }
        }
    }

    private void validate() {

        FilenameFilter conllFilter = (dir, name) -> name.toLowerCase().contains(".conll");

        File[] files = new File(validationPath).listFiles(conllFilter);
        if(files == null)
            return;
        Arrays.sort(files);

        validationRoot.removeAllChildren();

        for (File expectedFile : files) {
            try {
                File inFile = new File(new File(inPath), expectedFile.getName());
                File outFile = new File(new File(outPath), expectedFile.getName());

                Main.convertFile(gui.deptreeViewPane.getTransducer(), inFile, outFile);

                Root generated = fileToTree(outFile);
                Root expected = fileToTree(expectedFile);
                Root original = fileToTree(inFile);
                TreeConversionStats stats = new TreeConversionStats(generated, original);
                TreeComparator tc = new TreeComparator(expected, generated);

                tc.compare();
                stats.check();

                validationRoot.add(new DefaultMutableTreeNode(new ComparisonResult(outFile, inFile, expectedFile, stats, tc)));
            } catch (Exception e) {
                // cannot check this file...
                // will continue with next one regardless
                logger.info("could not compare file " + expectedFile.getName() + ", continuing with next one");
            }
        }

        sortTree();
        ((DefaultTreeModel)gui.treeModel).setRoot(root);
        ((DefaultTreeModel)gui.treeModel).reload(root);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == gui.tree) {
            if(!SwingUtilities.isRightMouseButton(e)) {
                return;
            }

            int row = gui.tree.getClosestRowForLocation(e.getX(), e.getY());
            gui.tree.setSelectionRow(row);

            DefaultMutableTreeNode fnode = (DefaultMutableTreeNode) gui.tree.getLastSelectedPathComponent();

            if(fnode.isLeaf() && fnode.getUserObject() instanceof ConversionResult) {
                ConversionResult cr = (ConversionResult) fnode.getUserObject();

                JPopupMenu menu = new JPopupMenu();

                // always give "show original" option
                JMenuItem showFirstTree = new JMenuItem("show original tree");
                showFirstTree.addActionListener(a ->
                        gui.setTree(fileToTree(cr.getSrcFile()), defaultNodeClassifier));
                menu.add(showFirstTree);

                // when tree is also in converted folder, give "show converted" option
                if(cr.isConverted()) {
                    JMenuItem showConvTree = new JMenuItem("show converted tree");
                    showFirstTree.addActionListener(a ->
                            gui.setTree(fileToTree(cr.getFile()), defaultNodeClassifier));
                    menu.add(showConvTree);

                    // add "add to validation set" option
                    File validationFile = new File(new File(validationPath), cr.getFile().getName());
                    if(!validationFile.exists()) {
                        JMenuItem addToGold = new JMenuItem("add to validation set");
                        addToGold.addActionListener(a -> {
                            try {
                                Files.copy(cr.getFile().toPath(), validationFile.toPath());
                            } catch (IOException e1) {
                                // there was an error during file writing
                                logger.error("error in writing file to validation folder " + validationPath);
                                logger.error(e1);
                            }
                        });
                        menu.add(addToGold);
                    }
                }

                // if tree is in validation set, also give "show expected" option
                if(cr instanceof ComparisonResult) {
                    JMenuItem showExpectedTree = new JMenuItem("show expected tree");
                    showExpectedTree.addActionListener(a ->
                            gui.setTree(fileToTree(((ComparisonResult) cr).getGoldFile()), defaultNodeClassifier));
                    menu.add(showExpectedTree);

                    // and option to show a comparing popup
                    JMenuItem compare = new JMenuItem("compare");
                    compare.addActionListener(a ->
                            new DeptreeComparePopup(gui.frame,
                                    TreeReaderWriter.fileToTree(cr.getFile()),
                                    TreeReaderWriter.fileToTree(((ComparisonResult) cr).getGoldFile())));
                    menu.add(compare);

                    // and option to remove from validation set
                    JMenuItem removeValidation = new JMenuItem("remove from validation set");
                    removeValidation.addActionListener(a -> ((ComparisonResult) cr).getGoldFile().delete());
                    menu.add(removeValidation);

                    // and option to overwrite validation set entry, if it differs
                    if(!cr.getSuccessful()) {
                        JMenuItem updateValidation = new JMenuItem("overwrite validation set");
                        updateValidation.addActionListener(a -> {
                            try {
                                Files.copy(cr.getFile().toPath(), ((ComparisonResult) cr).getGoldFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
                            } catch (IOException e1) {
                                // there was an error during file writing
                                logger.error("error in writing file to validation folder " + validationPath);
                                logger.error(e1);
                            }
                        });
                        menu.add(updateValidation);
                    }
                }

                menu.show(e.getComponent(), e.getX(), e.getY());

                return;
            }

            if(fnode.getParent() != null && fnode.getParent().toString().equals("conversions by rule")) {
                showRuleMenu(e, fnode);
            }
            
            if(fnode.toString().equals("conversions by rule")) {
                showFilterMenu(e);
            }

        }
    }

    private void showRuleMenu(MouseEvent e, DefaultMutableTreeNode fnode) {
        JPopupMenu menu = new JPopupMenu();
        JMenuItem visualizeRule = new JMenuItem("visualize rule");
        visualizeRule.addActionListener(a -> {
            // parse search expression

            String fullRule = fnode.getUserObject().toString().split(":-")[0];
            String[] ruleComponents = fullRule.split("->");
            if(ruleComponents.length != 2) {
                // something went wrong with rule string?
                return;
            }

            // transform rule expression into query tree
            ANTLRInputStream input = new ANTLRInputStream(ruleComponents[0]);
            TransducerLexer lexer = new TransducerLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            TransducerParser parser = new TransducerParser(tokens);
            TransducerParser.MatchTreeContext context =  parser.matchTree(new HashMap<>());
            Tree queryTree = context.tree;


            input = new ANTLRInputStream(ruleComponents[1]);
            lexer = new TransducerLexer(input);
            tokens = new CommonTokenStream(lexer);
            parser = new TransducerParser(tokens);
            TransducerParser.ReplacementNodeContext replacementNodeContext = parser.replacementNode();
            ReplacementNode replacementTree = replacementNodeContext.tree;

            // infer missing structure by creating a dummy rule ... really dirty hack, sorry!
            Rule dummyRule = new Rule(queryTree, replacementTree, null, "");

            Root beforeRoot = new DefaultRoot(new DefaultDocument());
            Node beforeTree = beforeRoot.getNode();
            structNodeToDefaultNode(beforeTree, queryTree.root);

            Root afterRoot = new DefaultRoot(new DefaultDocument());
            Node afterTree = afterRoot.getNode();
            while(replacementTree.getParent() != null) {
                replacementTree = replacementTree.getParent();
            }

            replacementNodeToDefaultNode(beforeRoot, afterTree, replacementTree);

            new DeptreeComparePopup(gui.frame, beforeRoot, afterRoot);
        });
        menu.add(visualizeRule);

        menu.show(e.getComponent(), e.getX(), e.getY());
    }

    private void replacementNodeToDefaultNode(Root beforeRoot, Node defaultNode, ReplacementNode replacementNode) {
        for(ReplacementNode child: replacementNode.getChildren()) {
            Node newChild = defaultNode.createChild();
            List<String> catchalls = child.getCatchAllVars();
            newChild.setDeprel(child.getDepRel() == null ? "*" : child.getDepRel());
            newChild.setForm(child.getName() == null ? "" : child.getName());

            // try to find POS and deprel tag from context
            newChild.setXpos("");
            if(child.getName() != null) {
                beforeRoot.getDescendants().stream().filter(n -> child.getName().equals(n.getForm())).findAny().ifPresent(n -> {
                    newChild.setXpos(n.getXpos() == null ? "" : n.getXpos());
                    if(child.getDepRel() == null || child.getDepRel().equals("")) {
                        newChild.setDeprel(n.getDeprel() == null ? "*" : n.getDeprel());
                    }
                });
            }

            newChild.setHead("");
            newChild.setFeats("");
            newChild.setLemma("");
            newChild.setMisc("");

            for(String catchAll: catchalls) {
                if(catchAll != null && catchAll.length() > 0) {
                    Node catchAllNode = newChild.createChild();
                    catchAllNode.setDeprel("catch*");
                    catchAllNode.setForm("?" + catchAll);
                    catchAllNode.setXpos("");
                    catchAllNode.setHead("");
                    catchAllNode.setFeats("");
                    catchAllNode.setLemma("");
                    catchAllNode.setMisc("");
                }
            }

            replacementNodeToDefaultNode(beforeRoot, newChild, child);
        }
    }

    private void structNodeToDefaultNode(Node defaultNode, StructNode structNode) {
        for(StructNode child: structNode.getChildren()) {
            Node newChild = defaultNode.createChild();
            String catchAll = child.getCatchallVar();
            if(child instanceof MatchingNode) {
                MatchingNode mnode = (MatchingNode) child;
                newChild.setDeprel(mnode.getDepRel() == null ? "" : mnode.getDepRel());
                newChild.setForm(mnode.getName() == null ? "" : mnode.getName());
                newChild.setXpos(mnode.getPosTag() == null ? "" : mnode.getPosTag());
            } else {
                newChild.setDeprel("*");
                newChild.setForm("FRONTIER");
                newChild.setXpos("");
            }

            if(catchAll != null && catchAll.length() > 0) {
                Node catchAllNode = newChild.createChild();
                catchAllNode.setDeprel("catch*");
                catchAllNode.setForm("?" + catchAll);
                catchAllNode.setXpos("");
                catchAllNode.setHead("");
                catchAllNode.setFeats("");
                catchAllNode.setLemma("");
                catchAllNode.setMisc("");
            }

            newChild.setHead("");
            newChild.setFeats("");
            newChild.setLemma("");
            newChild.setMisc("");
            structNodeToDefaultNode(newChild, child);
        }
    }

    /**
     * filters the entries in the subtree grouped by rules used.
     * There are three filtering options:
     * - filter by rule: only subtrees with a rule containing the given string are shown
     *    example: "n:SUBJ() -> n:nsubj();" will only show that very specific rule
     * - filter by structure: only shows trees where at least one subtree (
     *    (either unconverted or converted tree) matches the given structure
     *    example: "n:SUBJ()" will only show trees that have at least one "SUBJ" deprel.
     * - filter by sentence: only shows trees where the annotated sentence contains the given string
     *    example: "weniger als" will only show annotated sentences containing the string "weniger als".
     * @param e
     */
    private void showFilterMenu(MouseEvent e) {
        JPopupMenu menu = new JPopupMenu();
        JMenuItem filterRules = new JMenuItem("filter rules");
        filterRules.addActionListener(a -> {
            String filterText = JOptionPane.showInputDialog(gui.frame, "filter rules for: ");

            CursorManager.waitForTask(gui.frame, () -> {
                ruleRoot.removeAllChildren();

                if(filterText == null || filterText.equals("")) {
                    for(Rule rule: rules.keySet()) {
                        ruleRoot.add(rules.get(rule));
                    }
                } else {
                    for(Rule rule: rules.keySet()) {
                        if(rule.toString().contains(filterText)) {
                            ruleRoot.add(rules.get(rule));
                        }
                    }
                }
                ((DefaultTreeModel)gui.treeModel).reload();
                sortTree();
            });

        });
        menu.add(filterRules);

        JMenuItem filterStructure = new JMenuItem("filter structure");
        filterStructure.addActionListener(a -> {
            String filterText = JOptionPane.showInputDialog(gui.frame, "filter sentences for: ");

            CursorManager.waitForTask(gui.frame, () -> {
                if(filterText == null || filterText.equals("")) {
                    ruleRoot.removeAllChildren();
                    for (Rule rule: rules.keySet()) {
                        ruleRoot.add(rules.get(rule));
                    }
                } else {
                    ArrayList<DefaultMutableTreeNode> nodes = new ArrayList<>();

                    Collections.list(ruleRoot.children()).stream().parallel().forEach(treeNode -> {

                        DefaultMutableTreeNode currNode = (DefaultMutableTreeNode) treeNode;
                        Enumeration<TreeNode> children = currNode.children();
                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(currNode.getUserObject().toString());

                        while (children.hasMoreElements()) {
                            DefaultMutableTreeNode currChild = (DefaultMutableTreeNode) children.nextElement();
                            ConversionResult res = (ConversionResult) currChild.getUserObject();

                            if(CoNLLMatcher.checkForMatching(filterText, res.getSrcFile()) || CoNLLMatcher.checkForMatching(filterText, res.getFile())) {
                                newNode.add(new DefaultMutableTreeNode(res));
                            }

                        }
                        if(newNode.getChildCount() > 0) {
                            synchronized (nodes) {
                                nodes.add(newNode);
                            }
                        }
                    });

                    ruleRoot.removeAllChildren();

                    for(DefaultMutableTreeNode node: nodes) {
                        ruleRoot.add(node);
                    }
                }
                ((DefaultTreeModel)gui.treeModel).reload();
                sortTree();
            });
        });
        menu.add(filterStructure);

        JMenuItem filterSentence = new JMenuItem("filter sentence");
        filterSentence.addActionListener(a -> {
            String filterText = JOptionPane.showInputDialog(gui.frame, "filter sentences for: ");
            CursorManager.waitForTask(gui.frame, () -> {
                if(filterText == null || filterText.equals("")) {
                    ruleRoot.removeAllChildren();
                    for (Rule rule: rules.keySet()) {
                        ruleRoot.add(rules.get(rule));
                    }
                } else {
                    ArrayList<DefaultMutableTreeNode> nodes = new ArrayList<>();

                    Collections.list(ruleRoot.children()).stream().parallel().forEach(treeNode -> {
                        DefaultMutableTreeNode currNode = (DefaultMutableTreeNode) treeNode;
                        Enumeration<TreeNode> children = currNode.children();

                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(currNode.getUserObject().toString());
                        while (children.hasMoreElements()) {
                            DefaultMutableTreeNode currChild = (DefaultMutableTreeNode) children.nextElement();
                            ConversionResult res = (ConversionResult) currChild.getUserObject();

                            if(CoNLLMatcher.checkForString(filterText, res.getSrcFile()) || CoNLLMatcher.checkForString(filterText, res.getFile())) {
                                newNode.add(new DefaultMutableTreeNode(res));
                            }

                        }
                        if(newNode.getChildCount() > 0)
                            synchronized (nodes) {
                                nodes.add(newNode);
                            }

                    });

                    ruleRoot.removeAllChildren();

                    for(DefaultMutableTreeNode node: nodes) {
                        ruleRoot.add(node);
                    }
                }
                ((DefaultTreeModel)gui.treeModel).reload();
                sortTree();
            });
        });
        menu.add(filterSentence);

        menu.show(e.getComponent(), e.getX(), e.getY());
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void setTransducerPath(String transducerPath) {
        this.transducerPath = transducerPath;
        try {
            Transducer transducer = pathToTransducer(transducerPath);
            //Transducer.logger.setLevel(Level.WARN);
            gui.deptreeViewPane.setTransducer(transducer);
        } catch (IOException e) {
            logger.warn("cannot set transducer!");
            logger.error(e);
        }
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
        Transducer.logger.setLevel(verbose ? Level.ALL : Level.WARN);
    }

    // sort Conversion Results after index and rules / frontiers alphabetically
    public void sortTree() {
        Enumeration e = root.depthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            if (!node.isLeaf()) {
                sortNode(node);
            }
        }
    }

    private static Comparator<DefaultMutableTreeNode> tnc = (n1, n2) -> {
        Object o1 = n1.getUserObject();
        if(o1 instanceof ConversionResult) {
            return ((ConversionResult) o1).compareTo(n2.getUserObject());
        }
        return o1.toString().compareTo(n2.getUserObject().toString());
    };

    public static void sortNode(DefaultMutableTreeNode parent) {
        int n = parent.getChildCount();
        List<DefaultMutableTreeNode> children = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            children.add((DefaultMutableTreeNode) parent.getChildAt(i));
        }
        children.sort(tnc);
        parent.removeAllChildren();
        for (MutableTreeNode node: children) {
            parent.add(node);
        }
    }

    public void setValidationPath(String validationPath) {
        this.validationPath = validationPath;
    }
}
