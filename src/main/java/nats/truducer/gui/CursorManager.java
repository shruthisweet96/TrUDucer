package nats.truducer.gui;

import javax.swing.*;
import java.awt.*;

/**
 * this shows a waiting cursor while some task is running.
 */
public class CursorManager {

    public static void waitForTask(JFrame frame, BGTask bgTask) {
        frame.setCursor(Cursor.WAIT_CURSOR);

        // force cursor change!
        frame.toFront();
        frame.validate();
        frame.repaint();

        try {
            bgTask.apply();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            frame.setCursor(Cursor.DEFAULT_CURSOR);
        }
    }
}

interface BGTask {
    void apply();
}
