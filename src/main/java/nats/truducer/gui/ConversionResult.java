package nats.truducer.gui;

import com.google.common.base.CharMatcher;
import nats.truducer.deprel.TreeConversionStats;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * stores relevant information about a conversion.
 */
public class ConversionResult implements Comparable {
    private File file;
    private File srcFile;
    private List<String> blockers;
    protected boolean successful;
    private boolean converted;

    private int index;

    public ConversionResult(File file, File srcFile, TreeConversionStats stats, boolean converted) {
        this.file = file;
        this.srcFile = srcFile;
        this.blockers = new ArrayList<>();
        this.successful = stats.isTreeFullyConverted();
        this.index = Integer.parseInt("0" + CharMatcher.digit().retainFrom(srcFile.getName()));
        this.converted = converted;

        stats.getBlockerNodes().forEach(a -> blockers.add(a.getDeprel()));
    }

    @Override
    public String toString() {
        return String.format("%20s :     ",file.getName()) + String.join(", ", blockers);
    }

    public File getFile() {
        return file;
    }

    public File getSrcFile() {
        return srcFile;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof ConversionResult) {
            return Integer.compare(this.index, ((ConversionResult) o).index);
        }
        return toString().compareTo(o.toString());
    }

    public boolean getSuccessful() {
        return successful;
    }

    public boolean isConverted() {
        return converted;
    }
}
